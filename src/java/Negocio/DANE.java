/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.DepartamentoDAO;
import DAO.MunicipioDAO;
import DAO.PersonaDAO;
import DTO.Departamento;
import DTO.Municipios;
import DTO.Persona;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author madar
 */
public class DANE {

    PersonaDAO per = new PersonaDAO();
    MunicipioDAO mun = new MunicipioDAO();
    DepartamentoDAO dpto = new DepartamentoDAO();

    public DANE() {
    }

    /**
     * Reglas del negocio
     *
     * @return un listado de las personas en la BD
     */
    public String getListadoPersonas() {

        String msg = "\n<table>";
        msg += "\n<tr>"
                + "\n<td> Nombre </td>"
                + "\n<td> Cedula</td>"
                + "\n<td> E-mail</td>"
                + "\n<td> Dirección</td>"
                + "\n<td> Municipio</td>"
                + "\n<td> Departamento</td>"
                + "\n</tr>";
        List<Persona> myLista = per.read();
        for (Persona p : myLista) {

            msg += "\n<tr>"
                    + "\n<td>" + p.getNombre() + "</td>"
                    + "\n<td>" + p.getCedula() + "</td>"
                    + "\n<td>" + p.getEmail() + "</td>"
                    + "\n<td>" + p.getDireccion() + "</td>"
                    + "\n<td>" + p.getIdMunicipio().getNombre() + "</td>"
                    + "\n<td>" + p.getIdMunicipio().getIdDpto().getNombre() + "</td>"
                    + "\n</tr>";

        }
        return msg + "\n</table>";
    }
    
    public Boolean existePersona(int cedula){
        
       List<Persona> all = per.read();
       Boolean existe = false;
       
       for(Persona p: all){
           
           if(p.getCedula() == cedula){
           return true;
           }
           
       }
       
    return existe;
    }
    
    public List<Municipios> getMunicipios_dpto(int dpto){
        List<Municipios> all = mun.read();
        List<Municipios> munDpto = new ArrayList();
        
        for(Municipios m: all){
            if(m.getIdDpto().getIdDpto() == dpto){
                munDpto.add(m);
            }
        }
        
        return munDpto;
    }
    
    public List<Municipios> getMun() {

        return mun.read();
    }

    public Departamento getDepto(int id) {

        return dpto.readDepartamento(id);

    }

    public List<Departamento> getDeptos() {

        return dpto.read();
    }
    
    public void registrarPersona(Persona persona){
        per.create(persona);
    }
    
    public Persona buscarPersona(int cedula){
        return per.readPersona(cedula);
    }
    
    public Municipios buscarMun(int id){
        return mun.readMunicipio(id);
    }
    
    public Persona buscarPer(String nombre){
        PersonaDAO persona = new PersonaDAO();
        Persona per = null;
        List<Persona> personas= new ArrayList();
        personas = persona.read();
        for(Persona p: personas){
            if(p.getNombre().equals(nombre))
                per = p;
        }
    return per;
    }
}
