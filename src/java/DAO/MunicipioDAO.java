/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Municipios;
import Persistencia.MunicipiosJpaController;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Génesis
 */
public class MunicipioDAO {
    
     MunicipiosJpaController mun;
     
    public MunicipioDAO(){
        Conexion con =Conexion.getConexion();
        mun = new MunicipiosJpaController(con.getBd());
    }
    public void create(Municipios d){
        try {
            mun.create(d);
        } catch (Exception ex) {
            Logger.getLogger(MunicipioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Municipios> municipiosDe(int id_dpto){
    
        List<Municipios> municipio = mun.findMunicipiosEntities();
        List<Municipios> municipiosDpto = new ArrayList();
        
        municipio.stream().filter(m -> (m.getIdDpto().getIdDpto() == id_dpto)).forEachOrdered(m -> {
            municipiosDpto.add(m);
         });
        
        return municipiosDpto;
    }
    
    public List<Municipios> read(){
        return mun.findMunicipiosEntities();
    }
    
    public Municipios readMunicipio(int id_mun){
        return mun.findMunicipios(id_mun);
    }
    
    public void update(Municipios d){
        try {
            mun.edit(d);
        } catch (Exception ex) {
            Logger.getLogger(MunicipioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void destroy(int id){
        try {
            mun.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MunicipioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
