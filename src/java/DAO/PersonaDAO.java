/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Persona;
import Persistencia.PersonaJpaController;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Génesis
 */
public class PersonaDAO {

    PersonaJpaController per;
    
    public PersonaDAO(){
         Conexion con = Conexion.getConexion();
        per = new PersonaJpaController(con.getBd());
    }
    
    public void create(Persona p){
        try {
            per.create(p);
        } catch (Exception ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Persona> read(){
        return per.findPersonaEntities();
    }
    
    public Persona readPersona(int cedula){
        return per.findPersona(cedula);
    }
    
    public void update(Persona p){
        try {
            per.edit(p);
        } catch (Exception ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void destroy(int cedula){
        try {
            per.destroy(cedula);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
