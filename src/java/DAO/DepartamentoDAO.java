/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Departamento;
import Persistencia.DepartamentoJpaController;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Génesis
 */
public class DepartamentoDAO {
    
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    DepartamentoJpaController dpto;
    
    public DepartamentoDAO(){
    
        Conexion con = Conexion.getConexion();
        dpto = new DepartamentoJpaController(con.getBd());
    }
    public void create(Departamento departamento){
        try {
            dpto.create(departamento);
        } catch (Exception ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Departamento> read(){
        return dpto.findDepartamentoEntities();
    }
    
    public Departamento readDepartamento(int id_dpto){
        return dpto.findDepartamento(id_dpto);
    }
    
    public void update(Departamento departamento){
        try {
            dpto.edit(departamento);
        } catch (Exception ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void destroy(int id_dpto){
        try {
            dpto.destroy(id_dpto);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}