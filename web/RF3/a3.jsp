<%-- 
    Document   : a3
    Created on : 17/06/2021, 1:01:46 p. m.
    Author     : Génesis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Persona</title>
    </head>
    <body>
        <form action="../a333.do" method="get">
            <label>Por favor digite el nombre de la persona a consultar</label>
            <input type="text" name="nombre"/>
            <input type="submit" name="consultar" />
            <div class="clear"></div> 
        </form>   
    </body>
</html>
