<%-- 
    Document   : a2
    Created on : 17/06/2021, 1:01:06 p. m.
    Author     : Génesis
--%>

<%@page import="DAO.DepartamentoDAO"%>
<%@page import="DTO.Departamento"%>
<%@page import="DTO.Departamento"%>
<%@page import="Persistencia.PersonaJpaController"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar persona</title>
    </head>
    <body>

        <h4>Escoja al departamento al cuál desea consultar su información</h4>

        <div id="contenido">
            <form action="aa21.do" method="get">
                <input type="submit" name="consultar" />
                <select name="item">
                    <%  
                    
                     List<Departamento> dep = (List)(request.getSession().getAttribute("departamentos"));
                     String msg="";
                        for (Departamento p: dep) {
                            msg += "<option value="+'"'+p.getIdDpto()+'"'+">"+ p.getNombre() + "</option>\n";
                        }
                    %>
                    <%=msg%>
                </select>
               
                <h2 id="mensaje">
                    
                </h2>
                <div class="clear"></div> 
            </form>   
        </div>
    </body>
</html>

