<%-- 
    Document   : Error-a1
    Created on : 15/06/2021, 3:46:45 p. m.
    Author     : Génesis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>¡ERROR!</title>
    </head>
    <body>
        <h1>
           <%
               String msg = request.getSession().getAttribute("msg").toString();
           %>
           <%=msg%>
        </h1>
    </body>
</html>
