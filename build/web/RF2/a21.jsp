<%-- 
    Document   : a21
    Created on : 17/06/2021, 1:01:18 p. m.
    Author     : Génesis
--%>

<%@page import="DTO.Departamento"%>
<%@page import="Negocio.DANE"%>
<%@page import="java.util.List"%>
<%@page import="DTO.Municipios"%>
<%@page import="DTO.Municipios"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cantidad de habitantes por municipio</title>
    </head>
    <body>
        <h1>Información: </h1>
        <table>
            <tr>
                <td>Municipio</td>
                <td>Cantidad de personas</td>
            <tr>
                <%
                    int depto = Integer.parseInt(request.getSession().getAttribute("dpto").toString());
                    DANE dane = new DANE();
                    Departamento mun = dane.getDepto(depto);
                    List<Municipios> muni = dane.getMunicipios_dpto(depto);
                    String msg = "";

                    for (Municipios m : muni) {
                        msg += "<tr>";
                        msg += "<td>" + m.getNombre() + "</td>";
                        msg += "<td>"+ m.getPersonaCollection().size()+ "</td>";
                        msg += "<tr>";
                    }
                %>
                
                <%=msg%>
            </table>
    </body>
</html>
