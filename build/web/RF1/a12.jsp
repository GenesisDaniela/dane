<%-- 
    Document   : a12
    Created on : 17/06/2021, 12:58:09 p. m.
    Author     : Génesis
--%>

<%@page import="Negocio.DANE"%>
<%@page import="DTO.Departamento"%>
<%@page import="DTO.Municipios"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Persona</title>

    </head>
    <body>
        <h1>
            <%
                int depto = Integer.parseInt(request.getSession().getAttribute("dpto").toString());
                DANE dane = new DANE();
                Departamento mun = dane.getDepto(depto);
                String msg = "El Departamento elegido es " + mun.getNombre();
            %>

            <%=msg%>
        </h1>

        <div>
            <form class="center" action="./a12.do" method="get">
                <label> Digite Cédula: </label> 
                <input type="number" name="cedula" />
                <br>
                <label> Digite el nombre: </label>
                <input type="text" name="nombre" />
                <br>
                <label> Digite el email:  </label> 
                <input type="text" name="email" />
                <br>
                <label> Digite la dirección: </label> 
                <input type="text" name="direccion"/>
                <br>
                <label> Digite el municipio </label> 
                <br>
                <select name="item">
                    <%

                        List<Municipios> muni = dane.getMunicipios_dpto(depto);
                        msg = "";
                        for (Municipios p : muni) {
                            msg += "<option value=" + '"' + p.getIdMunicipio() + '"' + ">" + p.getNombre() + "</option>\n";
                        }
                    %>
                    <%=msg%>
                </select>

                <input type="submit"  value="Registrar"/>
            </form>   
        </div>
    </body>
</html>
