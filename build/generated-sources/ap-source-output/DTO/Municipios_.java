package DTO;

import DTO.Departamento;
import DTO.Persona;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-06-17T16:45:34")
@StaticMetamodel(Municipios.class)
public class Municipios_ { 

    public static volatile CollectionAttribute<Municipios, Persona> personaCollection;
    public static volatile SingularAttribute<Municipios, Integer> idMunicipio;
    public static volatile SingularAttribute<Municipios, Departamento> idDpto;
    public static volatile SingularAttribute<Municipios, String> nombre;

}