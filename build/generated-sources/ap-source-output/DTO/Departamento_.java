package DTO;

import DTO.Municipios;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-06-17T16:45:34")
@StaticMetamodel(Departamento.class)
public class Departamento_ { 

    public static volatile SingularAttribute<Departamento, Integer> idDpto;
    public static volatile SingularAttribute<Departamento, String> nombre;
    public static volatile CollectionAttribute<Departamento, Municipios> municipiosCollection;

}